# General Problem Solver

class GPS:
    'general algorithm'
    def __init__(self, initial_state, goal_state):
        self.new = []
        self.open = [initial_state]
        self.closed = []
        self.goal_state = goal_state

    def __fail(self):
        return "error: nothing in OPEN list"

    def __succeed(self):
        return 0

    def expand(self, state):
        return []

    def step(self, depth):
        # if new is empty, fail the program
        if self.open == []:
            return self.__fail()
        else:
            # put first node of open in close, then remove it from open
            if depth:
                # depth first
                self.closed.append(self.open.pop())
            else:
                # breadth first
                self.closed.append(self.open[0])
                del self.open[0]
            # if the last node in closed, the one we just moved, is the goal_state, it's a success
            if self.closed[-1] == self.goal_state:
                return self.__succeed()
            else:
                # expand the first node in closed, finding all the moves that can be done, and that are not already in open or closed
                self.new = self.expand(self.closed[-1])
                for state in self.new:
                    if (state not in self.open) and (state not in self.closed):
                        self.open.append(state)
                self.new = []

class WaterJugsProblem(GPS):
    def expand(self, state):
        result = []
        # rules of the problem: two jugs, 4L and 3L, start with 0L, 0L, trying to reach 2L, 0L
        # we first make sure the state is legal
        if state[0] >= 0 and state[0] <= 4 and state[1] >= 0 and state[1] <= 3:
            # fill 4L jug
            result.append([4, state[1]])
            # fill 3L jug
            result.append([state[0], 3])
            # empty 4L jug
            result.append([0, state[1]])
            # empty 3L jug
            result.append([state[0], 0])
            # pour 4L jug in 3L jug
            new_state = [0, state[1] + state[0]]
            if new_state[1] > 3:
                new_state = [new_state[1] - 3, 3]
            result.append(new_state)
            # pour 3L jug in 4L jug
            new_state = [state[0] + state[1], 0]
            if new_state[0] > 4:
                new_state = [4, new_state[0] - 4]
            result.append(new_state)
            return result

class MissionariesAndCannibalsProblem(GPS):
    def expand(self, state):
        result = []
        print("---", state, "---")
        if state[0][0] >= 0 and state[1][0] >= 0 and state[2][0] >= 0 and state[0][0] + state[1][0] + state[2][0] <= 3 and state[0][1] >= 0 and state[1][1] >= 0 and state[2][1] >= 0 and state[0][1] + state[1][1] + state[2][1] <= 3 and state[1][0] + state[1][1] <= 2:
            # if boat is on the left side
            if state[3] == 0:
                # disembark everyone
                new_state = [[state[0][0] + state[1][0], state[0][1] + state[1][1]], [0, 0], [state[2][0], state[2][1], 1]]
                print("---", state, "---")
                # any combinations of legal moves
                for missionary in range(new_state[0][0] + 1):
                    for cannibal in range(new_state[0][1] + 1):
                        if missionary >= cannibal:
                            new_state2 = [[missionary, cannibal], [new_state[0][0] - missionary, new_state[0][1] - cannibal], [new_state[2][0], new_state[2][1]], 1]
                            if new_state2[0][0] >= 0 and new_state2[1][0] >= 0 and new_state2[2][0] >= 0 and new_state2[0][0] + new_state2[1][0] + new_state2[2][0] <= 3 and new_state2[0][1] >= 0 and new_state2[1][1] >= 0 and new_state2[2][1] >= 0 and new_state2[0][1] + new_state2[1][1] + new_state2[2][1] <= 3 and new_state2[1][0] + new_state2[1][1] <= 2:
                                result.append(new_state2)
            # if boat is on the right side
            elif state[3] == 1:
                # disembark everyone
                new_state = [[state[0][0], state[0][1]], [0, 0], [state[2][0] + state[1][0], state[2][1] + state[1][1], 0]]
                # any combinations of legal moves
                for missionary in range(new_state[2][0] + 1):
                    for cannibal in range(new_state[2][1] + 1):
                        if missionary >= cannibal:
                            new_state2 = [[new_state[0][0], new_state[0][1]], [new_state[2][0] - missionary, new_state[2][1] - cannibal], [missionary, cannibal], 0]
                            if new_state2[0][0] >= 0 and new_state2[1][0] >= 0 and new_state2[2][0] >= 0 and new_state2[0][0] + new_state2[1][0] + new_state2[2][0] <= 3 and new_state2[0][1] >= 0 and new_state2[1][1] >= 0 and new_state2[2][1] >= 0 and new_state2[0][1] + new_state2[1][1] + new_state2[2][1] <= 3 and new_state2[1][0] + new_state2[1][1] <= 2:
                                result.append(new_state2)
            print("---", result, "---")
            return result

water_jugs = WaterJugsProblem([0, 0], [2, 0])
result = None
while (result == None):
    print("OPEN\n", water_jugs.open, "\nCLOSED\n", water_jugs.closed)
    result = water_jugs.step(False)
print("OPEN\n", water_jugs.open, "\nCLOSED\n", water_jugs.closed)
if result == 0:
    print("success !")
else:
    print(result)

missionaries_cannibals = MissionariesAndCannibalsProblem([[3, 3], [0, 0], [0, 0], 0], [[0, 0], [0, 0], [3, 3], 1])
result = None
while (result == None):
    print("OPEN\n", missionaries_cannibals.open, "\nCLOSED\n", missionaries_cannibals.closed)
    result = missionaries_cannibals.step(True)
print("OPEN\n", missionaries_cannibals.open, "\nCLOSED\n", missionaries_cannibals.closed)
if result == 0:
    print("success !")
else:
    print(result)
